const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, `Name is required`]
            },
        status: {
            type: String,
            default: "pending"
        }
    }
)

module.exports = mongoose.model(`Task`, taskSchema);
//using module.exports, we allow Task model to be used outside of its current module