const express = require("express");

//router will handle client request (method & url)
const router = express.Router();

// const taskController = require('./../controllers/taskControllers')
const {createTask, 
    getAllTasks, 
    deleteTask, 
    updateTask,
    getTask,
    completeTask
} = require('./../controllers/taskControllers')

//create a task
router.post("/", async (req, res) => {
    //console.log(req.body) {"name":"watching"} //object
    try{
        //handle promise. function defined in controller file
        await createTask(req.body).then(result => res.send(result))
    }catch(err){
        res.status(400).json(err.message)
    }
})

//GET ALL TASKS
router.get('/', async(req, res) => {
    try{
        await getAllTasks().then(result=> res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//DELETE A TASK
// ':taskId -- can be any variable name, referring to id
router.delete('/:id/delete', async (req, res) =>{
    
    //console.log(typeof req.params) // returns an object
    //console.log(typeof req.params.id) // returns a string

    try{
        //console.log(req.params.id)
        await deleteTask(req.params.id).then(response => res.send(response))
    }catch(err){
        res.status(500).json(err)
    }
})

//UPDATE A TASK
router.put(`/`, async (req, res) => {
    //console.log(req.params.taskId)
    const id = req.params.taskId
    //console.log(req.body)
    
    //await updateTask(id, req.body).then(response => res.send(response))

    await updateTask(req.body.name, req.body).then(response => res.send(response))
})

//RETRIEVE SPECIFIC TASK
router.get('/:id', async(req, res) => {
    
    try{
        await getTask(req.params.id).then(result=> res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//UPDATE STATUS TO COMPLETE
router.put('/:id/complete', async (req, res) => {
    await completeTask(req.params.id).then(result => res.send(result))
})

module.exports = router;