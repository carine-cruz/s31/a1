const express = require('express');
const app = express();
const dotenv = require('dotenv');
dotenv.config();
const mongoose = require('mongoose');
const PORT = 3006;

//connect routes to index.js file
const taskRoutes = require('./routes/taskRoutes')

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});


const db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection error:'));
db.once("open", () => console.log(`Connected to database`)); //

//SCHEMA
    //moved the schema to models folder

//ROUTES
//http:localhost:3000/api/tasks - constant, root
app.use(`/api/tasks`, taskRoutes)

app.listen(PORT, () => console.log(`Server connected at port ${PORT}`));

