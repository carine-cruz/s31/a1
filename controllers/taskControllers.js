//const router = require("../routes/taskRoutes")
const Task = require(`./../models/Task`)

module.exports.createTask = async (reqBody) => {
    return await Task.findOne({name: reqBody.name}).then((result, err) => {
        //console.log(result) //document
 
        if (result != null && result.name == reqBody.name){
            //return `Duplicate Task found.`
            return true
        } else {
 
            if(result == null){
                let newTask = new Task({
                    name: reqBody.name
                });
 
            //use save method to create new document
            return newTask.save().then(result => result)
            }
        }
    })
}

module.exports.getAllTasks = async () => {

    return await Task.find().then((docs, err)=>{
        if (docs){
            return docs
        } else{
            return err
        }
    })
}

//DELETE TASK
module.exports.deleteTask = async (id) => {
    //const {id} = params //if route passed an object
    return await Task.findByIdAndDelete(id).then( result => {
        //console.log(id)
        try{
            if(result != null){
                return true
            }else {
                return false
            }
        }catch(err){
            return err
        }
    })
}

//UPDATE A TASK
module.exports.updateTask = async (name, reqBody) => {
    //console.log(reqBody)
     
    //return await Task.findByIdAndUpdate(id, {$set: reqBody}, {new:true}).then(result => result)

    return await Task.findOneAndUpdate({name: name},{$set: reqBody},{new:true}).then(response => {
        if (response == null){
            return {message: `No existing document`}
        } else {
            if(response != null){
                return response
            }
        }
    })
}

//ACTIVITY
//1 - get a specific task
module.exports.getTask = async (id) => {
    return Task.findById(id).then(response => {
        if (response == null){
            return `No document found`
        }else{
            return response
        }
    })
}

//2 - update a specific task to change status to complete
module.exports.completeTask = async (id) => {
    return Task.findByIdAndUpdate(id, {$set: {status: "complete"}},{new: true}).then(response=> {
        if (response == null){
            return `No document found`
        }else{
            return response
        }
    })
}